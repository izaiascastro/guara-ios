import 'package:flutter/material.dart';

import 'pages/login/login_page.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.red,
          backgroundColor: Colors.white
        ),
        home: LoginPage()
      );
    
  }
}
