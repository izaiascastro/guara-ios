import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guara/pages/bairros/bairros_page.dart';
import 'package:guara/pages/bairros/meus_bairros_page.dart';

import 'pages/login/login_page.dart';
import 'pages/login/usuario.dart';
import 'utils/nav.dart';

class DrawerList extends StatelessWidget {
  _header(user) {
    return UserAccountsDrawerHeader(
      accountName: Text(user.nome),
      accountEmail: Text(user.email),
      currentAccountPicture: CircleAvatar(
        backgroundImage: AssetImage(
          "assets/images/logo-menu.png",
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    Future<Usuario> future = Usuario.get();

    return SafeArea(
        child: Drawer(
            child: ListView(
      children: <Widget>[
        FutureBuilder<Usuario>(
          future: future,
          builder: (context, snapshot) {
            Usuario user = snapshot.data;

            return user != null ? _header(user) : Container();
          },
        ),
        ListTile(
          leading: Icon(Icons.home),
          title: Text("Início"),
          subtitle: Text("Página Inicial!"),
          trailing: Icon(Icons.arrow_forward),
          onTap: () => _loadBairros(context),
        ),
        ListTile(
          leading: Icon(Icons.star),
          title: Text("Meus Bairros"),
          subtitle: Text("Veja em quais bairros você está vinculado!"),
          trailing: Icon(Icons.arrow_forward),
          onTap: () => _loadMeusBairros(context),
        ),
        ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text("Sair"),
          subtitle: Text("Encerrar seção no aplicativo!"),
          trailing: Icon(Icons.arrow_forward),
          onTap: () => _onClickLogout(context),
        ),
        Container(margin: EdgeInsets.all(20), child: Text('Versão: 2.0.0'))
      ],
    )));
  }

  _onClickLogout(BuildContext context) {
    Usuario.clear();
    Navigator.pop(context);
    push(context, LoginPage(), replace: true);
  }

  _loadMeusBairros(BuildContext context) {
    push(context, MeusBairroPage(), replace: false);
  }

  _loadBairros(BuildContext context) {
    push(context, BairroPage(), replace: false);
  }
}
