import 'package:guara/pages/pedidos/pedidos_page.dart';
import 'package:guara/utils/nav.dart';
import 'package:flutter/material.dart';

import 'bairro.dart';
import 'bairro_page.dart';

class MeusBairrosListView extends StatelessWidget {
  List<Bairro> bairros;
  MeusBairrosListView(this.bairros);

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      title: Text('Meus Bairros'),
      
    ),
    body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage("assets/images/fnd.png"),
            fit: BoxFit.cover,
            alignment: Alignment.topCenter,
          ),
        ),
        padding: EdgeInsets.all(16),
        child: bairros.length >= 1 ? ListView.builder(
            itemCount: bairros != null ? bairros.length : 0,
            itemBuilder: (context, index) {
              Bairro b = bairros[index];

              return Card(
                  color: Colors.grey[200],
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                          flex: 1,
                          child: 
                          Icon(Icons.location_city),
                          ),
                          Expanded(
                            flex: 5,
                            child: 
                          Text(
                            b.descricao,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 22),
                          ),
                          )
                        ],
                      )));
            }) :
            Center(child: Text('Sem dados disponíveis!',style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),),)
            
            ),) ;
  }
}
