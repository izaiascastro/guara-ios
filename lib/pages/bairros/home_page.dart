import 'package:flutter/material.dart';

import '../../drawer_list.dart';
import 'bairros_api.dart';
import 'bairros_page.dart';
import 'package:guara/utils/firebase.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin<HomePage> {
  @override
  void initState() {
    initFcm();
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      ),
      body: BairroPage(),
      drawer: DrawerList(),
    );
  }
}
