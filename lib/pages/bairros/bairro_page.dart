import 'package:guara/widgets/text.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

import 'bairro.dart';
import 'loripsum_bloc.dart';

class BairroPageOLD extends StatefulWidget {
  Bairro bairro;
  BairroPageOLD(this.bairro);

  @override
  _BairroPageOLDState createState() => _BairroPageOLDState();
}

class _BairroPageOLDState extends State<BairroPageOLD> {
  final _bloc = LoripsumApiBloc();

  void initState() {
    super.initState();

    _bloc.loadData();

  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.bairro.descricao),
        
      ),
      body: _body(),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
           _bloco1(),
           Divider(),
           _bloco2()
                   ],
                 ),
               );
             }

             Row _bloco1() {
               return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: <Widget>[
                             text(widget.bairro.descricao, fontSize:20, bold: true  ),
                           ],
                         ),
                      ],
                    );
             }

             void _onClickMap() {}

             void _onClickVideo() {}


             _bloco2() {
               return Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: <Widget>[
                   text('Pedidos de', fontSize: 16,bold: true ),
                   SizedBox(height: 20,),
                   StreamBuilder<String>(stream: _bloc.stream,
                   builder: (BuildContext context, AsyncSnapshot snapshot){
                     if(!snapshot.hasData){
                     return Center(child: Container(color: Colors.white, child: 
          Center(
            child: CircularProgressIndicator(),
          )
          ,) ,);
                     }
                     return text(snapshot.data, fontSize: 16);
                   })
                 ],
               );
             }
}
