import 'dart:async';

import 'package:guara/pages/bairros/simple_bloc.dart';

import 'bairro.dart';
import 'bairros_api.dart';


class BairrosBloc extends SimpleBloc<List<Bairro>>{
  Future<List<Bairro>> loadData() async {
     try {
      List<Bairro> carros = await BairrosApi.getCarros();
      add(carros);
      return carros;
     }catch (e) {
       addError(e);
     }
    }

    Future<List<Bairro>> loadMeusBairros() async {
     try {
      List<Bairro> bairros = await BairrosApi.meusBairros();
      add(bairros);
      return bairros;
     }catch (e) {
       addError(e);
     }
    }

     
}