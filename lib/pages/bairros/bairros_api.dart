

import 'package:guara/pages/login/usuario.dart';

import 'bairro.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class TipoCarro {
  static final String classicos = "clássicos";
  static final String esportivos = "esportivos";
  static final String luxo = "luxo";
}
class BairrosApi {

  static Future<List<Bairro>> getCarros() async  {

    Usuario user = await Usuario.get();

    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${user.token}"
    };

    print(headers);

   var url = 'http://guara.cajusistemas.com.br/loadBairros/${user.id}';

   var response = await http.get(url, headers: headers);

   String json = response.body;
   List list = convert.json.decode(json);

  //  final carros = List<Carro>();

  //  for(Map map in list) {
  //    Carro c = Carro.fromJson(map);
  //    carros.add(c);
  //  }

  final carros = list.map<Bairro>((map) => Bairro.fromJson(map)).toList();




   return carros;

   }

  static Future<List<Bairro>> meusBairros() async  {

    Usuario user = await Usuario.get();

    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${user.token}"
    };

    print(headers);

   var url = 'http://guara.cajusistemas.com.br/meusBairros/${user.id}';

   var response = await http.get(url, headers: headers);

   String json = response.body;
   List list = convert.json.decode(json);

  //  final carros = List<Carro>();

  //  for(Map map in list) {
  //    Carro c = Carro.fromJson(map);
  //    carros.add(c);
  //  }

  final bairros = list.map<Bairro>((map) => Bairro.fromJson(map)).toList();




   return bairros;

   }
}