
import 'loripsum_api.dart';
import 'simple_bloc.dart';

class LoripsumApiBloc extends SimpleBloc<String> {

 static String lorinCache;
 
 loadData() async {
     try {

      String loripsum = lorinCache ?? await LoripsumApi.getLoripsum();
      
      lorinCache = loripsum;

      add(loripsum);
     }catch (e) {
       addError(e);
     }
    }

}