import 'package:guara/pages/pedidos/pedidos_page.dart';
import 'package:guara/utils/nav.dart';
import 'package:flutter/material.dart';

import '../../drawer_list.dart';
import 'bairro.dart';
import 'bairro_page.dart';

class BairrosListView extends StatelessWidget {
  List<Bairro> bairros;
  BairrosListView(this.bairros);

  Widget build(BuildContext context) {
     return Scaffold(
       drawer: DrawerList(),
      appBar: AppBar(
      title: Text('Distribuidora Guará'),
      
    ),
    body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/fnd.png"),
            fit: BoxFit.cover,
            alignment: Alignment.topCenter,
          ),
        ),
        padding: EdgeInsets.all(16),
        child: bairros.length >= 1 ? ListView.builder(
            itemCount: bairros != null ? bairros.length : 0,
            itemBuilder: (context, index) {
              Bairro b = bairros[index];

              return Card(
                  color: b.pedidosAbertos > 0
                      ? Colors.orangeAccent[100]
                      : Colors.grey[200],
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            b.descricao,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 22),
                          ),
                          Text(
                            '${b.pedidosAbertos} Pedidos para entregar',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 16),
                          ),
                          ButtonBar(
                            children: <Widget>[
                              b.pedidosAbertos > 0
                                  ? FlatButton(
                                      color: Colors.green,
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.visibility),
                                          Text(' VISUALIZAR PEDIDOS')
                                        ],
                                      ),
                                      onPressed: () =>
                                          push(context, PedidosPage(b)),
                                    )
                                  : Text('')
                            ],
                          ),
                        ],
                      )));
            }) :
            Center(child: Text('Usuário não está vinculado aos bairros!',style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),),)
            
            ));
  }
}
