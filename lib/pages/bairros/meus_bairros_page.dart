import 'package:guara/widgets/text_error.dart';
import 'package:flutter/material.dart';
import 'package:guara/utils/firebase.dart';

import 'bairro.dart';
import 'bairros_bloc.dart';
import 'bairros_listview.dart';
import 'meus_bairros.dart';

class MeusBairroPage extends StatefulWidget {

  @override
  _MeusBairroPageState createState() => _MeusBairroPageState();
}

class _MeusBairroPageState extends State<MeusBairroPage>
    with AutomaticKeepAliveClientMixin<MeusBairroPage> {
  List<Bairro> bairros;

  final _bloc = BairrosBloc();

  void initState() {
    super.initState();

    _bloc.loadMeusBairros();
  }

  Widget build(BuildContext context) {
    super.build(context);

    return StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
          return TextError("Não foi possível buscar os bairros, verifique a sua conexão com a internet e tente novamente!");
        }

        if (!snapshot.hasData) {
          return Container(color: Colors.white, child: 
          Center(
            child: CircularProgressIndicator(),
          )
          ,) ;
        }

        List<Bairro> bairros = snapshot.data;

        return RefreshIndicator(
            child: MeusBairrosListView(bairros), onRefresh: _onRefresh);
      },
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  void dispose() {
    super.dispose();

    _bloc.dispose();
  }

  Future<void> _onRefresh() {
    return _bloc.loadMeusBairros();
  }
}
