class Bairro {
  int id;
  String descricao;
  int pedidosAbertos;

  Bairro(
      {this.id,
      this.descricao,
      this.pedidosAbertos,
      });

  Bairro.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    descricao = json['descricao'];
    pedidosAbertos = json['pedidosAbertos'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['descricao'] = this.descricao;
    data['pedidosAbertos'] = this.pedidosAbertos;
    return data;
  }
}