

import 'package:guara/api_response.dart';
import 'package:guara/pages/login/usuario.dart';
import 'package:guara/pages/pedidos/Pedido.dart';

import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class PedidosApi {

  static Future<List<Pedido>> getPedidos(int bairro) async  {

    Usuario user = await Usuario.get();

    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${user.token}"
    };

    print(headers);

   var url = 'http://guara.cajusistemas.com.br/loadPedidos/${bairro}';

   var response = await http.get(url, headers: headers);

   String json = response.body;
   List list = convert.json.decode(json);

  final carros = list.map<Pedido>((map) => Pedido.fromJson(map)).toList();


   return carros;

   }

   static Future<ApiResponse> marcarEntrega(Pedido pedido) async  {

     try {

     Usuario user = await Usuario.get();

    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${user.token}"
    };

    print(headers);

   var url = 'http://guara.cajusistemas.com.br/marcarEntrega/${pedido.id}';

   var response = await http.get(url, headers: headers);

    if(response.statusCode == 200){
      print('deu certo');
      return ApiResponse.ok();
    }

    return ApiResponse.error(msg: 'erro'); 

  
   }catch (error, exception) {
      print("Erro no login $error > $exception");

      return ApiResponse.error(msg:"Não foi possível fazer o login.");
    }

   }

   static Future<ApiResponse> salvarIdApp(String token) async  {

     try {

     Usuario user = await Usuario.get();

    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${user.token}"
    };

    print(headers);

   var url = 'http://guara.cajusistemas.com.br/idApp/${user.id}/${token}';

   var response = await http.get(url, headers: headers);

    if(response.statusCode == 200){
      print('deu certo salvar o idapp');
      return ApiResponse.ok();
    }

    return ApiResponse.error(msg: 'erro'); 

  
   }catch (error, exception) {
      print("Erro no login $error > $exception");

      return ApiResponse.error(msg:"Não foi possível fazer o login.");
    }

   }

}