import 'package:guara/pages/bairros/bairro.dart';
import 'package:guara/pages/pedidos/Pedido.dart';
import 'package:guara/pages/pedidos/pedidos_bloc.dart';
import 'package:guara/pages/pedidos/pedidos_listview.dart';
import 'package:guara/widgets/text_error.dart';
import 'package:flutter/material.dart';
import 'package:guara/utils/firebase.dart';


class PedidosPage extends StatefulWidget {
  Bairro bairro;
  PedidosPage(this.bairro);

  @override
  _PedidosPageState createState() => _PedidosPageState();
}

class _PedidosPageState extends State<PedidosPage>
    with AutomaticKeepAliveClientMixin<PedidosPage> {
  List<Pedido> pedidos;

  final _bloc = PedidosBloc();

  void initState() {
    super.initState();


    _bloc.loadData(widget.bairro.id);
  }

  Widget build(BuildContext context) {
    super.build(context);

    return StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
          return TextError("Não foi possível buscar os pedidos, verifique a sua conexão com a internet e tente novamente!");
        }

        if (!snapshot.hasData) {
          return Container(
            color: Colors.white,
            child: Center(
            child: CircularProgressIndicator(),
          )
          ) ;
        }

        List<Pedido> pedidos = snapshot.data;

        return RefreshIndicator(
            child: PedidosListView(pedidos, widget.bairro), onRefresh: _onRefresh);
      },
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  void dispose() {
    super.dispose();

    _bloc.dispose();
  }

  Future<void> _onRefresh() {
    return _bloc.loadData(widget.bairro.id);
  }
}
