import 'package:guara/pages/bairros/bairro.dart';
import 'package:guara/pages/bairros/bairros_page.dart';
import 'package:guara/pages/bairros/home_page.dart';
import 'package:guara/pages/pedidos/Pedido.dart';
import 'package:guara/pages/pedidos/pedidos_api.dart';
import 'package:guara/pages/pedidos/pedidos_page.dart';
import 'package:guara/pages/pedidos/pedidos_service.dart';
import 'package:guara/utils/alert.dart';
import 'package:guara/utils/nav.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../api_response.dart';

class PedidosListView extends StatelessWidget {
  List<Pedido> pedidos;
  Bairro bairro;
  PedidosListView(this.pedidos, this.bairro);

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(bairro.descricao),
      ),
      body: _lista(),
    );
  }

  _lista() {
    return Container(
       decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/fnd.png"),
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter,
                    ),
                  ),
        padding: EdgeInsets.all(16),
        child: ListView.builder(
            itemCount: pedidos != null ? pedidos.length : 0,
            itemBuilder: (context, index) {
              Pedido p = pedidos[index];

             
                            return Card(
                                color: Colors.grey[200],
                                child: Container(
                                    padding: EdgeInsets.all(20),
                                    child: Column(
                                      children: <Widget>[
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Cod. Pedido: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.id != null
                                              ? '${p.id}'
                                              : 'Não Informado',
                                          maxLines: 2,
                                          overflow: TextOverflow.visible,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Cliente: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.cliente != null
                                              ? '${p.cliente}'
                                              : 'Não Informado',
                                          maxLines: 2,
                                          overflow: TextOverflow.visible,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Logradouro: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.logradouro != null
                                              ? '${p.logradouro}'
                                              : 'Não Informado',
                                          maxLines: 2,
                                          overflow: TextOverflow.visible,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Nº: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.numero != null
                                              ? '${p.numero}'
                                              : 'Não Informado',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Ponto de Referência: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.pontoReferencia != null
                                              ? '${p.pontoReferencia}'
                                              : 'Não Informado',
                                          maxLines: 2,
                                          overflow: TextOverflow.visible,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Complemento: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.complemento != null
                                              ? '${p.complemento}'
                                              : 'Não Informado',
                                          maxLines: 2,
                                          overflow: TextOverflow.visible,
                                        )
                                          )
                                        ],),
                                       
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Quant. de Garafões: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.qtdGarafoes != null
                                              ? '${p.qtdGarafoes}'
                                              : 'Não Informado',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Valor Unitário: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.valorUnit != null
                                              ? '${p.valorUnit}'
                                              : 'Não Informado',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Valor Total: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.valorTotal != null
                                              ? '${p.valorTotal}'
                                              : 'Não Informado',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Forma de Pagamento: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.id != null
                                              ? _formPagamento(p.formaPagamento)
                                              : 'Não Informado',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                          )
                                        ],),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: 
                                            Text('Observação: ', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold))
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: 
                                            Text(
                                          p.obs != null
                                              ? '${p.obs}'
                                              : 'Dado não informado',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                          )
                                        ],),
                                        ButtonBar(
                                          children: <Widget>[
                                            FlatButton(
                                              color: Colors.blue,
                                child: Row(children: <Widget>[
                                  Icon(Icons.map),
                                  Text(' Ver No Mapa')
                                ],) ,
                                onPressed: () => {_verLocalizacao(p, context)},
                                                              ),
                                                            ],
                                                          ),
                                                          ButtonBar(
                                                                          children: <Widget>[
                                                                            FlatButton(
                                                                              color: Colors.green,
                                                                child: Row(children: <Widget>[
                                                                  Icon(Icons.check_circle),
                                                                  Text(' Marcar como Entregue')
                                                                ],) ,
                                                                onPressed: () => {_marcarEntrega(p, context)},
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      )));
                                            }));
                                  }
                                
                                  Future<void> _marcarEntrega(Pedido p, context) async {
                                
                                    Pedido pedido = p;
                                
                                    ApiResponse response = await PedidosApi.marcarEntrega(pedido);
                                
                                    if (response.ok) {
                                      mensagem(context, "PEDIDO ENTREGUE COM SUCESSO!");
                                      // push(context, HomePage(), replace: true);
                                    } else {
                                      print('Erro');
                                      mensagem(context, "Erro ao Entregar Pedido, verifique sua conexão com a internet e  tente novamente!");
                                    }
                                  }
                                
                                  
                                mensagem(BuildContext context , String msg) {
                                  showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (context) {
                                      return WillPopScope(
                                        onWillPop: () async => false,
                                        child: AlertDialog(
                                          title: Text("Status do Pedido"),
                                          content: Text(msg) ,
                                          actions: <Widget> [
                                            
                                            FlatButton(
                                              child: Text('OK'),
                                              onPressed: () {
                                                push(context, BairroPage(), replace: true);
                                              },
                                            )
                                          ]
                                        )
                                      );
                                    }
                                  );
                                }
                                
                                _formPagamento(String dado) {
                                  switch (dado) {
                                    case "CC":
                                    return dado = "Cartão de Crédito";
                                    break;
                                    case "D":
                                    return dado = "Dinheiro";
                                    break;
                                    case "CD":
                                    return dado = "Cartão de Débido";
                                    break;
                                    default:
                                    return dado = 'Não Informado';
                                  }
                                }
                                
                                  _verLocalizacao (Pedido p, BuildContext context) async {
                                    Pedido pedido = p;
                                    if(pedido.maps != null){
                                      if (await canLaunch(pedido.maps)) {
                                        await launch(pedido.maps);
                                      } else {
                                        throw 'Não foi possível abrir o mapa $pedido.maps';
                                      }
                                    }else{
                                      alert(context, 'Não existe mapa disponível para esse pedido');
                                    }
                                  }

}
