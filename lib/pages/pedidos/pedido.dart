class Pedido {
  int id;
  int bairroId;
  String logradouro;
  String situacao;
  String celular;
  String pontoReferencia;
  String complemento;
  int numero;
  int qtdGarafoes;
  String valorUnit;
  String valorTotal;
  String formaPagamento;
  String cliente;
  String obs;
  String maps;

  Pedido(
      {this.id,
      this.bairroId,
      this.logradouro,
      this.situacao,
      this.celular,
      this.pontoReferencia,
      this.complemento,
      this.numero,
      this.qtdGarafoes,
      this.valorUnit,
      this.valorTotal,
      this.formaPagamento,
      this.cliente,
      this.obs,
      this.maps,
      });

  Pedido.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bairroId = json['bairro_id'];
    logradouro = json['logradouro'];
    situacao = json['situacao'];
    celular = json['celular'];
    pontoReferencia = json['ponto_referencia'];
    complemento = json['complemento'];
    numero = json['numero'];
    qtdGarafoes = json['qtd_garafoes'];
    valorUnit = json['valor_unit'];
    valorTotal = json['valor_total'];
    formaPagamento = json['forma_pagamento'];
    cliente = json['cliente'];
    obs = json['obs'];
    maps = json['maps'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bairroId'] = this.bairroId;
    data['logradouro'] = this.logradouro;
    data['situacao'] = this.situacao;
    data['celular'] = this.celular;
    data['pontoReferencia'] = this.pontoReferencia;
    data['complemento'] = this.complemento;
    data['numero'] = this.numero;
    data['qtdGarafoes'] = this.qtdGarafoes;
    data['valorUnit'] = this.valorUnit;
    data['valorTotal'] = this.valorTotal;
    data['formaPagamento'] = this.formaPagamento;
    data['cliente'] = this.cliente;
    data['obs'] = this.obs;
    data['maps'] = this.maps;
    return data;
  }
}