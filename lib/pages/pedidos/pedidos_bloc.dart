import 'dart:async';

import 'package:guara/pages/bairros/simple_bloc.dart';
import 'package:guara/pages/pedidos/Pedido.dart';
import 'package:guara/pages/pedidos/pedidos_api.dart';



class PedidosBloc extends SimpleBloc<List<Pedido>>{
  Future<List<Pedido>> loadData(int bairro) async {
     try {
      List<Pedido> pedidos = await PedidosApi.getPedidos(bairro);
      add(pedidos);
      return pedidos;
     }catch (e) {
       addError(e);
     }
    }

   

     
}