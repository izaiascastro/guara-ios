import 'dart:async';

import 'package:guara/pages/bairros/bairros_page.dart';
import 'package:guara/pages/bairros/home_page.dart';
import 'package:guara/utils/alert.dart';
import 'package:guara/utils/firebase.dart';
import 'package:guara/utils/nav.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:guara/firebase/firebase_service.dart';

import '../../api_response.dart';
import 'app_button.dart';
import 'app_text.dart';
import 'login_bloc.dart';
import 'usuario.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  final _bloc = LoginBloc();

  final _tLogin = TextEditingController();

  final _tSenha = TextEditingController();

  final _focusSenha = FocusNode();

  @override
  void initState() {
    super.initState();

    Future<Usuario> future = Usuario.get();
    future.then((Usuario user) {
      if (user != null) {
        setState(() {
          push(context, BairroPage(), replace: true);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  _body() {
    return Form(
        key: _formKey,
        child: Container(
            child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Image.asset(
                  'assets/images/logomarca.png',
                  width: 200,
                ),
              ),
            ),
            Expanded(
                flex: 5,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/fnd.png"),
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0))),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[],
                            ),

                            AppText(
                              "E-mail",
                              "Digite o seu email",
                              controller: _tLogin,
                              validator: _validateLogin,
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              nextFocus: _focusSenha,
                            ),
                            SizedBox(height: 10),
                            AppText(
                              "Senha",
                              "Digite a senha",
                              controller: _tSenha,
                              password: true,
                              validator: _validateSenha,
                              keyboardType: TextInputType.text,
                              focusNode: _focusSenha,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            StreamBuilder<bool>(
                              stream: _bloc.stream,
                              builder: (context, snapshot) {
                                bool b = snapshot.data;
                                return AppButton(
                                  "Entrar",
                                  onPressed: _onClickLogin,
                                  showProgress: b ?? false,
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(margin: EdgeInsets.all(10), child: Text('Versão: 2.0.0'),)
                    ],
                  ),
                )),
          ],
        )));
  }

  Future<void> _onClickLogin() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    String login = _tLogin.text;
    String senha = _tSenha.text;

    print("Login: $login, Senha: $senha");

    ApiResponse response = await _bloc.login(login, senha);

    if (response.ok) {
      Usuario user = response.result;
      print(">>> $user");
      push(context, BairroPage(), replace: true);
    } else {
      alert(context, response.msg);
    }
  }

  String _validateLogin(String text) {
    if (text.isEmpty) {
      return "Digite o login";
    }
    return null;
  }

  String _validateSenha(String text) {
    if (text.isEmpty) {
      return "Digite a senha";
    }
    if (text.length < 3) {
      return "A senha precisa ter pelo menos 3 números";
    }
    return null;
  }

  @override
  void dispose() {
    super.dispose();

    _bloc.dispose();
  }

  Future<void> _onClickGoogle() async {
    final service = FirebaseService();
    ApiResponse response = await service.loginGoogle();

    if (response.ok) {
      push(context, BairroPage(), replace: true);
    } else {
      alert(context, response.msg);
    }
  }
}
