
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  String text;
  Function onPressed;
  bool showProgress;

  AppButton(this.text, {this.onPressed, this.showProgress = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 46,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0)),
        color: Colors.red,
        child: showProgress
        ? Center(child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),)
        : Text(
          text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 22
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
