import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../api_response.dart';
import 'usuario.dart';

class LoginApi {
  static Future<ApiResponse<Usuario>> login(String login, String senha) async {
    try {
      var url = 'http://guara.cajusistemas.com.br/api/auth/login';

      Map<String, String> headers = {"Content-Type": "application/json"};

      Map params = {'email': login, 'password': senha};

      String s = json.encode(params);
      print("> $s");

      var response = await http.post(url, body: s, headers: headers);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');

      Map mapResponse = json.decode(response.body);

      if (response.statusCode == 200) {
        final user = Usuario.fromJson(mapResponse);

        user.save();

        Usuario user2 = await Usuario.get();
        print('user2:: $user2');


        return ApiResponse.ok(result:user);
      }

      return ApiResponse.error(msg: mapResponse["error"]);
    } catch (error, exception) {
      print("Erro no login $error > $exception");

      return ApiResponse.error(msg:"Não foi possível fazer o login.");
    }
  }
}
