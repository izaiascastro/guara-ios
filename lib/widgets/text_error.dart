import 'package:flutter/material.dart';

class TextError extends StatelessWidget {
  String msg;
  TextError(this.msg);
  
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.all(16),
      child: Align(
        alignment: Alignment.center,
            child: Text(msg,
            style: TextStyle(color: Colors.red, fontSize: 15) ,),
          ));
  }
}