import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:guara/pages/pedidos/pedidos_api.dart';
import '../api_response.dart';

FirebaseMessaging fcm;

void initFcm() {
  if (fcm == null) {
    fcm = FirebaseMessaging();
  }

  fcm.getToken().then((token) {

    _sendToken(token);
   
    print("\n******\nFirebase Token $token\n******\n");

  });

  fcm.subscribeToTopic("all");

  fcm.configure(
    onMessage: (Map<String, dynamic> message) async {
      print('\n\n\n*** on message $message');
    },
    onResume: (Map<String, dynamic> message) async {
      print('\n\n\n*** on resume $message');
    },
    onLaunch: (Map<String, dynamic> message) async {
      print('\n\n\n*** on launch $message');
    },
  );

  if (Platform.isIOS) {
    fcm.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    fcm.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("iOS Push Settings: [$settings]");
    });
  }
}

 Future<void> _sendToken(String token) async {
   String tok = token;

    ApiResponse response = await PedidosApi.salvarIdApp(tok);

    if (response.ok) {
     print('token salvo com sucesso');
      // push(context, HomePage(), replace: true);
    } else {
      print('Erro');
      
    }
  }